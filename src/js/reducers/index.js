const reducer = (state = {}, action) => {
  console.log(`REDUCER ${action.type}`);
  switch (action.type) {
    case "GET_USERS_DATA":
      return {
        ...state,
        users: action.data,
        message: action.message
      };
    case "GET_USERS_PAGINATION":
      let usersPagination = {};
      usersPagination.currentPage = action.data.currentPage;
      usersPagination.perPage = action.data.perPage;
      usersPagination.pageCount = action.data.pageCount;
      usersPagination.rateLimit = action.data.rateLimit;
      return {
        ...state,
        usersPagination: usersPagination,
        message: action.message
      };
    case "GET_USER":
      return {
        ...state,
        user: action.data,
        message: action.message
      };
    case "GET_USER_ERROR":
      return {
        ...state,
        user: {error: action.data},
        message: action.message
      };
    case "ADD_USER":
      return {
        ...state,
        formDataSuccess: action.data,
        formDataError: [],
        message: action.message
      };
    case "ADD_USER_ERROR":
      return {
        ...state,
        formDataSuccess: {},
        formDataError: action.data,
        message: action.message
      };
    case "UPDATE_USER":
      return {
        ...state,
        formDataSuccess: action.data,
        formDataError: [],
        user: action.data,
        message: action.message
      };
    case "UPDATE_USER_ERROR":
      return {
        ...state,
        formDataSuccess: {},
        formDataError: action.data,
        message: action.message
      };
    case "DELETE_USER":
      return {
        ...state,
        formDataSuccess: {delete: true},
        formDataError: [],
        message: action.message
      };
    case "DELETE_USER_ERROR":
      return {
        ...state,
        formDataSuccess: [],
        formDataError: [],
        message: action.message
      };
    default:
      return state
  }
};

export default reducer;
