import "core-js/es6";

import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import PropTypes from "prop-types";

import {Provider} from "react-redux"
import {store} from "./store";

import Header from "components/header";
import Footer from "components/footer";
import Table from "components/table";
import User from "components/user";
import FormEdit from "components/form/edit";
import FormAdd from "components/form/add";

const Root = ({store}) => (
  <Provider store={store}>
    <Router>
      <div className={`wrapper`}>
        <div className={`container header-wrapper`}>
          <div className={`row`}>
            <Header/>
          </div>
        </div>
        <div className={`container main-wrapper`}>
          <div className={`row`}>
            <div className={`col-12`}>
              <Switch>
                <Route exact path="/" component={Table}/>
                <Route path="/page/:page" component={Table}/>
                <Route path="/users/new" component={FormAdd}/>
                <Route path="/users/:id/edit" component={FormEdit}/>
                <Route path="/users/:id" component={User}/>
              </Switch>
            </div>
          </div>
        </div>
        <div className={`container footer-wrapper`}>
          <Footer/>
        </div>
      </div>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
};

document.addEventListener("DOMContentLoaded", function (event) {
  ReactDOM.render(<Root store={store}/>, document.getElementById("app"));
});
