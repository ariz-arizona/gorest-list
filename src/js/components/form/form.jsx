import React, {Component} from "react";
import {Link} from "react-router-dom";
import moment from "moment";
import Input from "../../components/input";

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    //todo: front validation need
    //todo: inputs as form components
    //todo: initial values in form
    let {success, error, data, type} = this.props;
    if (!data) {
      data = {};
    }
console.log(success)
    return <React.Fragment>
      <form onSubmit={this.props.onSubmit.bind(this)}>
        <div className="row">
          <Input
            className="col-sm-12 col-md-6"
            type="text"
            label="Имя"
            placeholder="Введите имя"
            name="first_name"
            required={true}
            value={data["first_name"]}
          />
          <Input
            className="col-sm-12 col-md-6"
            type="text"
            label="Фамилия"
            placeholder="Введите фамилию"
            name="last_name"
            required={true}
            value={data["last_name"]}
          />
        </div>
        <div className="row">
          <Input
            className="col-sm-12 col-md-6"
            type="email"
            label="E-mail"
            placeholder="Введите e-mail"
            name="email"
            required={true}
            value={data["email"]}
          />
          <Input
            className="col-sm-12 col-md-6"
            type="phone"
            label="Телефон"
            placeholder="Введите номер телефон"
            name="phone"
            value={data["phone"]}
          />
        </div>
        <div className="row">
          <Input
            className="col-sm-12 col-md-6"
            type="date"
            label="Дата рождения"
            placeholder="Укажите дату рождения"
            name="dob"
            props={{max: moment().subtract(11, "year").format("YYYY-MM-DD")}}
            value={data["dob"]}
          />
          <Input
            className="col-sm-12 col-md-6"
            type="radio"
            label="Пол"
            name="gender"
            values={[
              {valueItem: "female", label: "Женский"},
              {valueItem: "male", label: "Мужской"},
            ]}
            value={data["gender"]}
          />
        </div>
        <div className="row">
          <Input
            className="col-sm-12 col-md-6"
            type="text"
            label="Сайт"
            placeholder="Укажите адрес сайта"
            name="website"
            value={data["website"]}
          />
          <Input
            className="col-sm-12 col-md-6"
            type="text"
            label="Аватар"
            placeholder="Укажите аватар (ссылка)"
            name="avatar"
            value={data["avatar"]}
          />
        </div>
        <div className="row">
          <Input
            className="col-sm-12"
            type="textarea"
            label="Адрес"
            placeholder="Укажите адрес"
            name="address"
            value={data["address"]}
          />
        </div>
        <div className="row">
          <Input
            className="col-sm-12"
            type="checkbox"
            label="Статус"
            name="status"
            values={[
              {label: "Активен"},
            ]}
            value={data["status"]}
          />
        </div>
        <div className="row">
          {type === "edit" ? (
            <React.Fragment>
              <div className="col-sm-6 form-group">
                <button type="submit" className="btn btn-primary btn-block">Изменить</button>
              </div>
              <div className="col-sm-6 form-group">
                <button type="submit" className="btn btn-danger btn-block"
                        onClick={this.props.onDelete.bind(this)}>Удалить
                </button>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="col-sm-12">
                <button type="submit" className="btn btn-primary btn-block">Добавить</button>
              </div>
            </React.Fragment>
          )}
        </div>
      </form>
      {
        Object.keys(success).length ? (
          <div className="p-3 mt-2 bg-success text-white">
            Был {type === "edit" ? "изменен" : "создан"} контакт <Link
            to={["", "users", success.id].join("/")}
            className={`text-white`}
            target={`_blank`}
          >{[success.first_name, success.last_name].join(" ")}</Link>
          </div>
        ) : null
      }
      {
        error.length ? (
          <div className="p-3 mt-2 bg-danger text-white">
            Контакт не создан
          </div>
        ) : null
      }
    </React.Fragment>
  }
}