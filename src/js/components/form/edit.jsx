import React, {Component} from "react";
import {connect} from "react-redux"
import {Link} from "react-router-dom";

;
import * as actions from "actions";
import Form from "./form";

class FormEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const {match: {params}} = this.props;
    if (params.id) {
      actions.getUser(params.id);
      this.setState({
        id: params.id
      });
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  onSubmit(event) {
    event.preventDefault();
    let {id} = this.state;
    let formData = new FormData(event.target);
    actions.updateUser(id, formData);
  }

  onDelete(event) {
    event.preventDefault();
    let {id} = this.state;
    actions.deleteUser(id);
  }

  render() {
    //todo: не нравится мне, как апдейт (не)работает на сервере

    let {error, success} = this.props;

    if (!this.props.user || !Object.keys(this.props.user).length) {
      return <h1>Loading...</h1>;
    }

    if (this.props.user.hasOwnProperty("error")) {
      return (
        <div>
          <h1>Ошибка!</h1>
          <p>Попробуйте вернуться на <Link to={"/"}>Главную страницу</Link> :) </p>
        </div>
      );
    }

    if (this.props.success.hasOwnProperty("delete") && this.props.success.delete === true) {
      return <div>
        <h1>Пользователь удален!</h1>
        <p>Возвращайтесь на <Link to={"/"}>Главную страницу</Link> :) </p>
      </div>
    }

    let data = this.props.user;
    data.status = data.status === "active";
    try {
      data.avatar = data._links.avatar.href;
    } catch (err) {
      data.avatar = ""
    }
    return (
      <div className={`user-form`}>
        <h1>Изменить данные <Link to={["", "users", data.id].join(" ")}
                                  target={`_blank`}>{[data["first_name"], data["last_name"]].join(" ")}</Link>
        </h1>
        <Form
          error={error}
          success={success}
          data={data}
          type={"edit"}
          onSubmit={this.onSubmit.bind(this)}
          onDelete={this.onDelete.bind(this)}
        />
      </div>
    );
  }
}

export default connect(
  (state) => ({
    error: state.formDataError || [],
    success: state.formDataSuccess || [],
    user: state.user || [],
  }),
)(FormEdit)