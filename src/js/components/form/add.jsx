import React, {Component} from "react";
import {connect} from "react-redux";
import * as actions from "actions";
import Form from "./form";

class FormAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  onSubmit(event) {
    event.preventDefault();
    let formData = new FormData(event.target);
    if (formData.get("status") === null) {
      formData.set("status", "inactive");
    } else if (formData.get("status") === "on") {
      formData.set("status", "active");
    }
    actions.addUser(formData);
  }

  render() {
    return (
      <div className={`user-form`}>
        <h1>Добавить контакт</h1>
        <Form {...this.props} onSubmit={this.onSubmit.bind(this)}/>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    error: state.formDataError || [],
    success: state.formDataSuccess || []
  }),
)(FormAdd)