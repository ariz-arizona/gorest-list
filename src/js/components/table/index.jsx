import React, {Component} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import * as actions from "actions";
import {lang} from "../../helpers/helpers"

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pagination: {
        currentPage: 1,
        perPage: 20,
        pageCount: 0,
        rateLimit: {limit: 0, remaining: 0, reset: 0}
      }
    };
  }

  componentWillMount() {
    actions.getUsers({page: this.props.match.params.page || 1});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.usersPagination && JSON.stringify(nextProps.usersPagination) !== JSON.stringify(this.state.pagination)) {
      this.setState({
        pagination: nextProps.usersPagination,
      })
    }
    //todo: _.isEqual
    if (nextProps.match.params && nextProps.match.params.page && this.props.match.params.page !== nextProps.match.params.page) {
      actions.getUsers({page: nextProps.match.params.page});
    }
  }

  changePage(event) {
    let value = event.target.value;
    let url = "/";
    if (value > 1) {
      url = ["", "page", value].join("/")
    }
    this.props.history.push(url, false);
    window.scrollTo(0,0);
  }

  renderSelectPageNumber() {
    let {currentPage, pageCount, perPage, rateLimit} = this.state.pagination;
    return (
      <React.Fragment>
        <Link
          to={["", "page", currentPage - 1].join("/")}
          className={`pagination-arrow ${currentPage === 1 ? "inactive" : ""}`}
        >
          <i className={`fa fa-chevron-left`}/>
        </Link>
        <select className={`pagination-select`} value={currentPage} onChange={this.changePage.bind(this)}>
          {((count) => {
            let result = [];
            for (let i = 1; i <= count; i++) {
              result.push(<option key={i}>{i}</option>);
            }
            return result;
          })(pageCount)}
        </select>
        <Link
          to={["", "page", currentPage + 1].join("/")}
          className={`pagination-arrow ${currentPage === rateLimit.limit ? "inactive" : ""}`}
        >
          <i className={`fa fa-chevron-right`}/>
        </Link>
      </React.Fragment>
    );
  }

  renderPagination() {
    //todo: pagination as pages!!
    //todo: pagination as component!!
    let {pageCount, perPage, rateLimit} = this.state.pagination;

    return <div className={`pagination`}>
      <div>Страница {this.renderSelectPageNumber()} из {pageCount}</div>
    </div>
  }

  render() {
    //todo: loading component!
    if (!this.props.users || !this.props.users.length) {
      return <h1>Loading...</h1>;
    }

    let {users} = this.props;
    let headers = ["id", "avatar", "name", "phone", "email", "website", "action"];
    return (
      <div className={`users-list`}>
        <div className={`table`}>
          <div className={`thead`}>
            <div className={`tr no-td-resize`}>
              {headers.map((el, i) => {
                return <div key={i} className={`td ${el}`}>{el !== "action" ? lang(el) : ""}</div>
              })}
            </div>
          </div>
          <div className={`tbody`}>
            {users.map((object, i) => {
              return (
                <div className={`tr no-td-resize`} key={i}>
                  {headers.map((el, j) => {
                    return <div key={j} className={`td ${el}`}>{
                      ((id) => {
                        switch (id) {
                          case "name":
                            return (
                              <Link to={["", "users", object.id].join("/")}>
                                {[object["first_name"], object["last_name"]].join(" ")}
                              </Link>
                            );
                          case "avatar":
                            return <img src={object["_links"][id]["href"] || "https://via.placeholder.com/84"} alt={`avatar`}/>;
                          case "website":
                            return <a title={object[id]} href={object[id]} target={`_blank`}>{object[id]}</a>;
                          case "email":
                            return <a title={object[id]} href={`mailto:${object[id]}`} target={`_blank`}>{object[id]}</a>;
                          case "phone":
                            return <a title={object[id]} href={`tel:${object[id]}`} target={`_blank`}>{object[id]}</a>;
                          case "action":
                            return (
                              <React.Fragment>
                                <a href={`#`} title={`Edit`}><i className={`fa fa-pencil`}/></a>
                                <a href={`#`} title={`Delete`}><i className={`fa fa-trash`}/></a>
                              </React.Fragment>
                            );
                          default:
                            return object[id];
                        }
                      })(el)
                    }</div>
                  })}
                </div>
              );
            })}
          </div>
          {this.renderPagination()}
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    length: (state.users || []).length,
    users: state.users || [],
    usersPagination: state.usersPagination
  }),
)(Table)