import React, {Component} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import * as actions from "actions";
import {lang} from "../../helpers/helpers"
import moment from "moment";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    moment.locale("ru");
    const {match: {params}} = this.props;
    if (params.id) {
      actions.getUser(params.id);
      this.setState({
        id: params.id
      });
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  deleteUser(e) {
    e.preventDefault();
    let {id} = this.state;
    //todo: и вот это в dispatch вроде привязывать надо
    actions.deleteUser(id);
  }

  deleteUserInfo(e) {
    e.preventDefault();
    this.setState({deleteInfo: !this.state.deleteInfo});
  }

  renderDeleteInfo() {
    let {first_name, last_name} = this.props.user;
    let {avatar} = this.props.user._links;
    return (
      <div className={`user-info`}>
        <div className="row">
          <div className="col-12 user-info-avatar">
            <div className="user-info-avatar-img">
              <img src={avatar.href || "https://via.placeholder.com/250"} alt="avatar"/>
            </div>
          </div>
          <div className="col-12 mt-1 text-center">
            <p>
              Вы точно хотите удалить контакт {[first_name, last_name].join(" ")}?
            </p>
            <p>
              <a href={`#`} className={`btn btn-danger mr-2`} onClick={this.deleteUser.bind(this)}>Да</a>
              <a href={`#`} className={`btn btn-secondary`} onClick={this.deleteUserInfo.bind(this)}>Нет</a>
            </p>
          </div>
        </div>
      </div>
    );
  }


  render() {
    //todo: delete alert as modal

    if (!this.props.user || !Object.keys(this.props.user).length) {
      return <h1>Loading...</h1>;
    }

    if (this.props.user.hasOwnProperty("error")) {
      return (
        <div>
          <h1>Ошибка!</h1>
          <p>Попробуйте вернуться на <Link to={"/"}>Главную страницу</Link> :) </p>
        </div>
      );
    }

    if (this.props.success.hasOwnProperty("delete") && this.props.success.delete === true) {
      return <div>
        <h1>Пользователь удален!</h1>
        <p>Возвращайтесь на <Link to={"/"}>Главную страницу</Link> :) </p>
      </div>
    }

    let user = this.props.user;
    let {first_name, gender, id, last_name, status} = this.props.user;
    let {avatar} = this.props.user._links;
    let infoList = ["first_name", "last_name", "email", "phone", "dob", "website", "address"];

    if (this.state.deleteInfo) {
      return this.renderDeleteInfo();
    }

    return (
      <div className={`user-info`}>
        <div className="row">
          <div className="col-12 col-md-4 user-info-avatar">
            <div className="user-info-avatar-img">
              <img src={avatar.href || "https://via.placeholder.com/250"} alt="avatar"/>
            </div>
            <div className="user-info-avatar-name">
              <i className={`fa fa-${gender === "male" ? "mars" : "venus"}`}/>
              {[first_name, last_name].join(" ")}
            </div>
            <div className="user-info-avatar-status">
              <i className={`fa fa-${status === "inactive" ? "ban" : status === "active" ? "check" : "question"}`}/>
              <span>{lang(`${gender === "male" ? "m" : "f"}_${status}`)}</span>
            </div>
            <div className="user-info-avatar-actions">
              <Link to={["", "users", id, "edit"].join("/")}><i className={`fa fa-pencil`}/> Редактировать</Link><br/>
              <a href={`#`} onClick={this.deleteUserInfo.bind(this)}><i className={`fa fa-trash`}/> Удалить</a>
            </div>
          </div>
          <div className="col-12 col-md-8 user-info-list">
            <dl>
              {infoList.map((el, i) => {
                if (!user[el]) {
                  return;
                }
                return <React.Fragment key={i}>
                  <dt>{lang(el)}</dt>
                  <dd>{
                    ((id) => {
                      switch (id) {
                        case "dob":
                          return moment(user[id]).isValid() ? moment(user[id]).format("LL") : user[id];
                        case "website":
                          return <a href={user.id} target="_blank">{user[id]}</a>;
                        case "email":
                          return <a href={`mailto:${user.id}`} target="_blank">{user[id]}</a>;
                        case "phone":
                          return <a href={`tel:${user.id}`} target="_blank">{user[id]}</a>;
                        default:
                          return user[id];
                      }
                    })(el)
                  }</dd>
                </React.Fragment>
              })}
            </dl>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    success: state.formDataSuccess || [],
    user: state.user || []
  }),
)(User)