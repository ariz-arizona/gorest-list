import React, {Component} from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

export default class Header extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg">
        <a className="navbar-brand" href="/">
          <img src="/img/logo.svg" height="48" alt={`logo`}/>
          <span>Liosta</span>
        </a>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to={["", "users", "new"].join("/")}>
              <i className={`fa fa-plus`}/>&nbsp;
              Добавить контакт
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}