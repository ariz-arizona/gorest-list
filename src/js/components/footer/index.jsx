import React, {Component} from "react";
import PropTypes from "prop-types";

export default class Footer extends React.Component {
  render() {
    return (
      <div className={`row footer`}>
        <div className={`col-sm-4`}>
          Компания «Liosta»
        </div>
        <div className={`col-sm-0 col-md-4`}>
        </div>
        <div className={`col-sm-8 col-md-4`}>
          <p className={`footer-info`}>
            Вся информация, представленная на сайте, носит ознакомительный характер.
          </p>
        </div>
      </div>
    );
  }
}