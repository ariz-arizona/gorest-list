import React from "react";
import {connect} from "react-redux";

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }

  componentWillMount() {
    this.setState({
      value: this.props.value || "",
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        value: nextProps.value || "",
      })
    }
  }

  onChange(e) {
    let {type, value} = e.target;
    switch (type) {
      case "checkbox":
        value = e.target.checked;
        break;
    }
    this.setState({value: value});
  }

  render() {
    //todo: set default value
    let {label, placeholder, name, type, className, required, values, errors, props} = this.props;
    let {value} = this.state;
    let id = `id_${name}`;
    let error = [];

    if (errors) {
      error = errors.filter(el => {
        return el.field === name;
      });
    }

    let validationClass = "";
    let validationMsg = {};
    if (error.length) {
      validationClass = "is-invalid";
      validationMsg = {msg: error[0].message, class: "invalid-feedback"};
    }
    let input;
    switch (type) {
      case "radio" :
        input = (
          <React.Fragment>
            <label>{label}</label>
            <div>
              {values.map((el, i) => {
                let {valueItem, label} = el;
                let id = `${name}_${i}`;
                return (
                  <div className={`form-check form-check-inline`} key={i}>
                    <input className={`form-check-input ${validationClass}`}
                           type="radio"
                           name={name}
                           id={id}
                           value={valueItem}
                           checked={value === valueItem}
                           {...props}
                           onChange={this.onChange.bind(this)}/>
                    <label className="form-check-label" htmlFor={id}>{label}</label>
                  </div>
                );
              })}
            </div>
          </React.Fragment>
        );
        break;
      case "checkbox" :
        input = (
          <React.Fragment>
            <label>{label}</label>
            {values.map((el, i) => {
              let {label} = el;
              let id = `${name}_${i}`;
              return (
                <div className={`form-check`} key={i}>
                  <input type="checkbox"
                         className={`form-check-input ${validationClass}`}
                         id={id}
                         name={name}
                         checked={value}
                         onChange={this.onChange.bind(this)}
                         {...props}/>
                  <label className="form-check-label" htmlFor={id}>{label}</label>
                </div>
              );
            })}
          </React.Fragment>
        );
        break;
      case "textarea":
        input = (
          <React.Fragment>
            <label htmlFor="address">{label}</label>
            <textarea className={`form-control ${validationClass}`}
                      name={name}
                      id={id}
                      placeholder={placeholder}
                      value={value}
                      onChange={this.onChange.bind(this)}
                      {...props}/>
          </React.Fragment>
        );
        break;
      default:
        input = (
          <React.Fragment>
            <label htmlFor={id}>{label}</label>
            <input type={type}
                   className={`form-control ${validationClass}`}
                   name={name}
                   id={id}
                   placeholder={placeholder} required={required}
                   value={value}
                   onChange={this.onChange.bind(this)}
                   {...props}/>
          </React.Fragment>
        );
    }

    let classes = ["form-group"];
    classes.push(className);
    if (error.length) {
      classes.push("is-invalid");
    }
    classes.push(`type_${type}`);
    return (
      <div className={[classes.join(" ")]}>
        {input}
        {validationMsg && <div className={validationMsg.class}>{validationMsg.msg}</div>}
      </div>
    );
  }
}

export default connect(
  (state) => ({
    errors: state.formDataError || [],
    success: state.formDataSuccess || []
  }),
)(Input)