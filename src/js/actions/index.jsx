import {get, post, put, del} from "../helpers/server";
import {store} from "../store";

/*
 * action types
 */
export const GET_USERS_DATA = "GET_USERS_DATA";
export const GET_USERS_DATA_ERROR = "GET_USERS_DATA_ERROR";

export const GET_USERS_PAGINATION = "GET_USERS_PAGINATION";

export const GET_USER = "GET_USER";
export const GET_USER_ERROR = "GET_USER_ERROR";

export const ADD_USER = "ADD_USER";
export const ADD_USER_ERROR = "ADD_USER_ERROR";

export const DELETE_USER = "DELETE_USER";
export const DELETE_USER_ERROR = "DELETE_USER_ERROR";

export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_USER_ERROR = "UPDATE_USER_ERROR";

/*
 * action creators
 */

export function getUsers(params = {}) {
  let data = Object.assign({}, params, {
    fields:
      ["id",
        "avatar",
        "first_name",
        "last_name",
        "phone",
        "email",
        "website"].join(",")
  });
  get("users", data).then(resp => {
    //todo: а сюда ли ставить dispatch?
    if (resp.data._meta.success === true) {
      store.dispatch({type: GET_USERS_DATA, data: resp.data.result});
      store.dispatch({type: GET_USERS_PAGINATION, data: resp.data._meta});
    } else {
      store.dispatch({type: GET_USERS_DATA_ERROR, data: {}});
    }
  }).catch(err => {
    console.log(err);
    store.dispatch({type: GET_USERS_DATA_ERROR, data: {}});
  })
}

export function getUser(id) {
  get(`users/${id}`, {}).then(resp => {
    if (resp.data._meta.success === true) {
      store.dispatch({type: GET_USER, data: resp.data.result});
    } else {
      store.dispatch({type: GET_USER_ERROR, data: {}});
    }
  }).catch(err => {
    console.log(err);
    store.dispatch({type: GET_USER_ERROR, data: {}});
  })
}

export function addUser(data) {
  post(`users`, data).then(resp => {
    if (resp.data._meta.success === true) {
      store.dispatch({type: ADD_USER, data: resp.data.result});
    } else {
      store.dispatch({type: ADD_USER_ERROR, data: resp.data.result});
    }
  }).catch(err => {
    console.log(err);
    store.dispatch({type: ADD_USER_ERROR, data: []});
  })
}

export function updateUser(id, data) {
  put(`users/${id}`, data).then(resp => {
    if (resp.data._meta.success === true) {
      store.dispatch({type: UPDATE_USER, data: resp.data.result});
    } else {
      store.dispatch({type: UPDATE_USER_ERROR, data: resp.data.result});
    }
  }).catch(err => {
    console.log(err);
    store.dispatch({type: DELETE_USER_ERROR, data: {}});
  })
}

export function deleteUser(id) {
  del(`users/${id}`, {}).then(resp => {
    if (resp.data._meta.success === true) {
      store.dispatch({type: DELETE_USER, data: resp.data.result});
    } else {
      store.dispatch({type: DELETE_USER_ERROR, data: resp.data.result});
    }
  }).catch(err => {
    console.log(err);
    store.dispatch({type: DELETE_USER_ERROR, data: {}});
  })
}
