import {translations} from "../data/lang";

export function getNoun(number, one, two, five) {
  number = Math.abs(number);
  number %= 100;
  if (number >= 5 && number <= 20) {
    return five;
  }
  number %= 10;
  if (number == 1) {
    return one;
  }
  if (number >= 2 && number <= 4) {
    return two;
  }
  return five;
}

export function lang(value) {
  let lang = "ru";
  try {
    return translations[value][lang];
  } catch (err) {
    console.log(`NO TRANSLATION FOR ${value}`);
    if (translations[value] && translations[value]["en"]) {
      return translations[value]["en"];
    } else {
      return value;
    }
  }
}