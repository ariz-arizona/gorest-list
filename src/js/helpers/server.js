import axios from "axios";
import {API_KEY, HOST} from "../config";

axios.defaults.baseURL = HOST;

if (API_KEY) {
  axios.defaults.headers["Authorization"] = API_KEY;
}

export const post = (action, data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(action, data)
      .then(function (resp) {
        resolve(resp);
      })
      .catch(function (error) {
        reject(error.response || "Error " + action);
      });
  });
};

export const get = (action, data) => {
  if (data) {
    data = {
      params: data,
    };
  }

  return new Promise((resolve, reject) => {
    axios
      .get(action, data)
      .then(function (resp) {
        resolve(resp);
      })
      .catch(function (error) {
        reject(error.response || "Error " + action);
      });
  });
};

export const put = (action, data) => {
  return new Promise((resolve, reject) => {
    axios
      .put(action, data)
      .then(function (resp) {
        resolve(resp);
      })
      .catch(function (error) {
        reject(error.response || "Error " + action);
      });
  });
};

export const del = (action, data) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(action, data)
      .then(function (resp) {
        resolve(resp);
      })
      .catch(function (error) {
        reject(error.response || "Error " + action);
      });
  });
};